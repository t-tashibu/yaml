# Openssl証明書の作成
openssl genrsa -out ubuntu.key 2048

# 作成したOpenssl証明書から公開鍵の作成を行い、User名との紐づけを行う。
openssl req -new -key ubuntu.key -out ubuntu.csr -subj "/CN=ubuntu"

# 作成した公開鍵をBase64に変換して、それを指定のYamlファイルに出力する
cat ubuntu.csr | base64 | tr -d "\n" >> fsutil-1.yaml
cat fsutil-1.yaml | sed "s/^/  request: /"  > fsutil-2.yaml

# kubernetesクラスターの認証局に登録する公開鍵のYamlファイルを作成し、Base64で変換したコードをrequest:として追加する
cat > ubuntu.yaml << EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: ubuntu
spec:
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
  - key encipherment
  - digital signature
EOF

cat fsutil-2.yaml >> ubuntu.yaml

rm fsutil-1.yaml
rm fsutil-2.yaml

# 公開鍵をApplyして、Applyした公開鍵に許可設定を行う
kubectl apply -f ubuntu.yaml
kubectl certificate approve ubuntu

# kubernetesクラスターにはTLS認証のためにPKI証明書が必要なため、認証局に登録した公開鍵のコードからPKI証明書の作成を行う。
kubectl get csr ubuntu -o jsonpath='{.status.certificate}'| base64 -d > ubuntu.crt

# kubeconfigファイルに作成した証明書、登録したUser、PKI証明書の登録を行う。
kubectl config set-credentials ubuntu --client-key=ubuntu.key --client-certificate=ubuntu.crt --embed-certs=true

# kubernetesのコンテキストファイルにUserと使用するkubernetesクラスター名の登録を行う。
kubectl config set-context ubuntu --cluster=kubernetes --user=ubuntu

# ここからは別のRBACのハンズオンを実施するために必要なNameSpaceやPod、ServiceAccountの作成になります。
kubectl create ns rbac-test

kubectl run --image=nginx rbac-pod --namespace=rbac-test

kubectl create sa -n rbac-test rbac-sa

cat > /etc/kubernetes/ubuntu.conf << EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data:
    server: 
  name: kubernetes
contexts:
- context:
    cluster:
    user:
  name: ubuntu
current-context: ubuntu
kind: Config
preferences: {}
users:
- name: ubuntu
  user:
    client-certificate-data:
    client-key-data:
EOF

cat > /root/cluster-add.sh << EOF
mkdir -p /home/ubuntu/.kube 
cp -i /etc/kubernetes/ubuntu.conf /home/ubuntu/.kube/config 
chown $(id -u):$(id -g) /home/ubuntu/.kube/config 
export KUBECONFIG=/home/ubuntu/.kube/config
EOF
