---
1：プロジェクトの初期化とベースの作成
# kubebuilder init --domain example.com --repo example.com/sampleweb
INFO Writing kustomize manifests for you to edit...
INFO Writing scaffold for you to edit...
INFO Get controller runtime:
$ go get sigs.k8s.io/controller-runtime@v0.19.4
～～～
INFO Update dependencies:
$ go mod tidy
～～～
Next: define a resource with:
$ kubebuilder create api
～～～
---

---
2:APIオブジェクトとコントローラーのひな型を作成
# kubebuilder create api --group stable --version v1 --kind FirstCrd
INFO Create Resource [y/n]
y
INFO Create Controller [y/n]
y
INFO Writing kustomize manifests for you to edit...
INFO Writing scaffold for you to edit...
INFO api/v1/firstcrd_types.go
INFO api/v1/groupversion_info.go
INFO internal/controller/suite_test.go
INFO internal/controller/firstcrd_controller.go
INFO internal/controller/firstcrd_controller_test.go
INFO Update dependencies:
$ go mod tidy
INFO Running make:
$ make generate
mkdir -p /root/sampleweb/bin
---

---
3:CRD定義の作成
---
