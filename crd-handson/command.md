```
.nanchattecc.sh 

creating myfirstcr pd
pod/myfirstcr created
```

```
⇒　別のターミナルを立ち上げて、このPodにアクセスしてみます。
   今回は、簡単にするために「kubectl port-forward」を使います。
# kubectl port-forward myfirstcr 10080:80
Forwarding from 127.0.0.1:10080 -> 80
Forwarding from [::1]:10080 -> 80
```

```
さらにもう1つターミナルを立ち上げて、Podへアクセスしてみます。

# curl http://localhost:10080
Hello,crd!!
```
