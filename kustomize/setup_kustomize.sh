#!/bin/bash

set -e  # エラーハンドリング: 途中でエラーが出たらスクリプトを停止

# 1. Kustomize のインストール (kubectl に含まれるため省略可能)
if ! command -v kustomize &> /dev/null; then
  echo "Kustomize が見つかりません。インストールします..."
  curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
fi

# 2. ディレクトリ構成の作成
echo "ディレクトリを作成中..."
mkdir -p my-kustomize-project/base
mkdir -p my-kustomize-project/overlays/dev
mkdir -p my-kustomize-project/overlays/prod

# 3. ベースマニフェストの作成
cat <<EOF > my-kustomize-project/base/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
        - name: my-app
          image: my-app:latest
          ports:
            - containerPort: 80
EOF

cat <<EOF > my-kustomize-project/base/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-app-service
spec:
  selector:
    app: my-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
EOF

cat <<EOF > my-kustomize-project/base/kustomization.yaml
resources:
  - deployment.yaml
  - service.yaml
EOF

# 4. Dev 環境の設定
cat <<EOF > my-kustomize-project/overlays/dev/kustomization.yaml
bases:
  - ../../base

patches:
  - path: patch-deployment.yaml
EOF

cat <<EOF > my-kustomize-project/overlays/dev/patch-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 2
  template:
    spec:
      containers:
        - name: my-app
          image: my-app:dev
EOF

# 5. Prod 環境の設定
cat <<EOF > my-kustomize-project/overlays/prod/kustomization.yaml
bases:
  - ../../base

patches:
  - path: patch-deployment.yaml
EOF

cat <<EOF > my-kustomize-project/overlays/prod/patch-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 5
  template:
    spec:
      containers:
        - name: my-app
          image: my-app:prod
EOF

# 6. 確認用メッセージ
echo "Kustomize 環境のセットアップが完了しました。"

# 7. デプロイ (オプション)
read -p "Dev 環境にデプロイしますか？ (y/n): " deploy_dev
if [[ "\$deploy_dev" == "y" ]]; then
  kubectl apply -k my-kustomize-project/overlays/dev
fi

read -p "Prod 環境にデプロイしますか？ (y/n): " deploy_prod
if [[ "\$deploy_prod" == "y" ]]; then
  kubectl apply -k my-kustomize-project/overlays/prod
fi

echo "完了しました。"
