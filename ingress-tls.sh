openssl req -x509 -sha256 -newkey rsa:4096 -keyout ca.key -out ca.crt -days 356 -nodes -subj '/CN=ingress.tls.jp'

openssl req -new -newkey rsa:4096 -keyout tls.key -out tls.csr -nodes -subj '/CN=ingress.tls.jp'

echo "subjectAltName = DNS:ingress.tls.jp" > san.txt

openssl x509 -req -sha256 -days 365 -in tls.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out tls.crt -extfile san.txt

kubectl create secret generic tls-secret --from-file=tls.crt=tls.crt --from-file=tls.key=tls.key

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/baremetal/deploy.yaml

cat > test-nginx.yml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: nginx-tls
  labels:
    run: nginx-tls
spec:
  containers:
  - name: nginx
    image: nginx
    lifecycle:
      postStart:
        exec:
          command: ["/bin/sh", "-c", "echo nginx application > /usr/share/nginx/html/index.html"]
---
apiVersion: v1
kind: Service
metadata:
  labels:
    run: nginx-tls
  name: nginx-tls
spec:
  ports:
  - port: 80
  selector:
    run: nginx-tls
EOF


kubectl apply -f test-nginx.yml


cat > nginx-tls.yml << EOF
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-app-tls
spec:
  ingressClassName: nginx
  rules:
  - host: ingress.tls.jp
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: nginx-tls
            port:
              number: 8080
  tls:
    - hosts:
      - ingress.tls.jp
      secretName: tls-secret 
EOF
