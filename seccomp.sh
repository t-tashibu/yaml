# Podの作成
cat > seccomp.yaml << EOF
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu
spec:
  containers:
    - name: ubuntu
      image: ubuntu:21.10
      command:
        - sleep
        - infinity
EOF

#YamlファイルのApplyとExecコマンドの実行
kubectl apply -f seccomp.yaml

kubectl exec -it ubuntu -- /bin/sh

#ファイル作成
mkdir sample-dir


