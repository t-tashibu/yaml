zabbix　インストール手順

#Localeの変更
localectl

localectl list-locales

apt update
apt install language-pack-ja -y

localectl set-locale LANG=ja_JP.UTF-8

#Zabbixインストール 6.4
wget https://repo.zabbix.com/zabbix/6.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.4-1+ubuntu22.04_all.deb
dpkg -i zabbix-release_6.4-1+ubuntu22.04_all.deb
apt update

apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-sql-scripts zabbix-agent -y

apt install mysql-server mysql-client apache2 -y
systemctl start mysql apache2
systemctl enable mysql apache2

mysqladmin -u root password

mysql -u root -p

create database zabbix character set utf8mb4 collate utf8mb4_bin;

create user zabbix@localhost identified by 'password';

grant all privileges on zabbix.* to zabbix@localhost;

set global log_bin_trust_function_creators = 1;

quit;

zcat /usr/share/zabbix-sql-scripts/mysql/server.sql.gz | mysql --default-character-set=utf8mb4 -u zabbix -p zabbix

mysql -uroot -p

set global log_bin_trust_function_creators = 0;

quit;

cp -p /etc/zabbix/zabbix_server.conf /etc/zabbix/zabbix_server.conf.org

vi /etc/zabbix/zabbix_server.conf
# # DBPassword= ⇒　DBPassword=password


systemctl restart zabbix-server zabbix-agent apache2
systemctl enable zabbix-server zabbix-agent

