# Node01のプライベートIP転記
aws ec2 describe-instances --query 'Reservations[].Instances[].{Env:Tags[?Key==`Environment`]|[0].Value,InstanceId:InstanceId,Name:Tags[?Key==`Name`]|[0].Value,PrivateIp:PrivateIpAddress,PublicIp:PublicIpAddress}' --output table  | grep cka-node-00-01 | cut -c 78-90 > node-1s.txt
echo " node01" >> node-1s.txt
sleep 10
tr -d '\n' < node-1s.txt > node01.txt

# Node02のプライベートIP転記
aws ec2 describe-instances --query 'Reservations[].Instances[].{Env:Tags[?Key==`Environment`]|[0].Value,InstanceId:InstanceId,Name:Tags[?Key==`Name`]|[0].Value,PrivateIp:PrivateIpAddress,PublicIp:PublicIpAddress}' --output table  | grep cka-node-00-02 | cut -c 78-90 > node-2s.txt
echo " node02" >> node-2s.txt
sleep 10
tr -d '\n' < node-2s.txt >> node02.txt

# HostへプライベートIPの転機
cat node01.txt >> /etc/hosts
cat node02.txt >> /etc/hosts