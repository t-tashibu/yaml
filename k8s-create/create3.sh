sudo apt-get update && sudo apt-get install -y apt-transport-https curl
sudo mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl

sudo kubeadm init --pod-network-cidr=10.244.0.0/16 2>&1 | tee /k8s-cluster-install.log 

mkdir -p /root/.kube 
cp -i /etc/kubernetes/admin.conf /root/.kube/config 
chown $(id -u):$(id -g) /root/.kube/config 
export KUBECONFIG=/root/.kube/config 

