sudo -i

hostnamectl set-hostname controlpalne01
hostnamectl set-hostname controlpalne02
hostnamectl set-hostname node01
hostnamectl set-hostname node02
hostnamectl set-hostname workstation

cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/authorized_keys

cat > .ssh/k8s.pem << EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEApkDbI6c6be7vzuDq7rBOLfNkhsg83HRq2gBLQr8R3x9WrpW5
yTY7SBRo7nQ80IL6Hh6KctFodK0WPeg0iHFTfoEhZyVxxDMdTKrKUOrTY8UTJ9m0
Hw5N1IpjeknIFZ+sYm/4UbqIsEf3/1965hCMoS8X35X1wFjD5tCf/kVbgkJH68F6
mkGHOWZpUHner9E8rURzpaRzlUYisnzpX2k9yCJjOo70dr6ptmx7pX/ZdKuQIyhX
UFombRnLZndSqdpQPplwKQdqxsx8kLciI/f5Hm5S7mD5lmnHtKt/yD/z8S3UHLLO
Vzo8Q9CcvZt3uv7WX7b8NB0THWdU+yt3nKPhlwIDAQABAoIBAFy2QD5yrFfP7jV9
IiY3dXGUkJPFKIN7/HZTwaSYLtwxesJjRrbZoleA4j9P3PYehGK1f1mmQ80FFSky
y9Dm+bvLQRhnPjk+k/FEMQzM11OE/Di15AeJj4eeIPl6GbVNVPGUJXNWRZGgoMDt
uvh/dB0SyQprcx9voNefsgk3Hl/VskZV98xlHMr7RHfGw0mRxI/i/kzQoNge4yqx
7iayLbekP/5lcSsI+tdrugTAdHmklhqZlLIt69ELOBX/ZoFEQqA3RYJFhfUAevdx
4rmJxVNCa5LMG/8VYx70IrRsEuQeDjkETbYVZ/nDDQi8OT1xvf+EaP4Hhx8TjUN8
QSHIdeECgYEA+YiilDjuO7DeaXlf10arZyAeUftoHAtoWfysSRdwYTzXncA2dSQ2
PqQetwqGdaUvFstSL6YOctzU730LBRfbTGXHMZo4zHI3YREgk5RZRpGnd/r+B7wV
Gl8r3J4GfyDXzj4Vbe0JwCv/o8fskaGiUKoaFtsMXfzxV0A0RprHxYcCgYEAqo/A
uJhVb+o7agC6lvnvIjvnoKBKISm41ROO0XavGzkl/9fndXo7/4E9an5rd2xDt4Pt
G3GYc6Ob0G/GwgaBsfAbBJVUBypupuYm0vVvu2dTFUb0XQJTJgzbt1zOiOGBPxVT
GdU77Ede8dvDmtYL7GHXT7QxRznlraja2J85B3ECgYAxhkO2JUJrCGRsDJHP+Q+l
rQM3hC0ae80fJv3Ol99O/TNR9McaIrC9Z/dMnsMm8DzTSlEqccumUlvUmD2qqhx1
IjIRjyk8smJ5WlvbznKVhez17BOk11hc3oynAZoKeys5nKrGIWhxAop/J/u0v56m
YcxBwwHGWaBQj8aQSMHjWwKBgGaJa1vOQLa6QyOz8ninIssa5mIbWtVbBsIaMubc
CeM81Qpc2so2RPGS2cA25hZyYLdnuVihTvxeJGMHf57PWJMHfMahm1V/fewaOsbi
RFyOflUxReDQb/pVCpXOVl/V6eRpxn6mQZMHSR3vd0WTXrGqFE3XTzS5qsle1Mob
1qYRAoGBALkB6rJIqgtDGEGV6qYBlk98xPIiTHtXm72MfEnce6J2FCei2fbKPpR0
9ARUqIiEWqGFGMAxSCYZwYYzJjtOzBrJUcY62fBoBacyOUtDI8772K1aqYPWh4f4
Ck575jYhQVSuvu3r2CEbhG6dJSk7vf/JVzGLar3qh/1DBJEiuc/1
-----END RSA PRIVATE KEY-----
EOF

chmod 600 .ssh/k8s-key.pem

cat > .ssh/config << EOF
Host *
 IdentityFile ~/.ssh/k8s.pem
 User root
EOF

172.31.xxx.xxx controlplane01.k8s.example.com cp1 controlpalne01
172.31.xxx.xxx controlplane02.k8s.example.com cp2 controlpalne02
172.31.xxx.xxx node01.k8s.example.com node01
172.31.xxx.xxx node02.k8s.example.com node02
172.31.xxx.xxx workstation.k8s.example.com workstation cp.k8s.example.com

apt update
apt install -y dnsmasq

cat > /etc/dnsmasq.d/dns.conf << EOF
no-resolv
server=8.8.8.8
address=/.cloudapps.k8s.example.com/172.31.xxx.xxx
bind-interfaces
listen-address=::1,127.0.0.1
listen-address=172.31.xxx.xxx
EOF

systemctl enable --now dnsmasq
systemctl status dnsmasq.service

cat > /etc/netplan/99-custom-dns.yaml << EOF
network:
  version: 2
  ethernets:
    eth0:
      nameservers:
        addresses: [172.31.xxx.xxx]
      dhcp4-overrides:
        use-dns: false
        use-domains: false
EOF

netplan apply
resolvectl status

ping controlplane01.k8s.example.com -c 5
ping controlplane02.k8s.example.com -c 5
ping node01.k8s.example.com -c 5
ping node02.k8s.example.com -c 5
ping workstation.k8s.example.com -c 5


. /etc/os-release

echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list

wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${VERSION_ID}/Release.key -O- | apt-key add -

apt update -y
apt install -y podman

mkdir -p /opt/registry/certs
mkdir -p /opt/registry/data

REGISTRY=/opt/registry/certs
KEY=${REGISTRY}/workstation-docker-distribution.key
openssl genrsa -out $KEY 2048

sed -i.bak -e \
 '/^\[ v3_ca \]/a subjectAltName=@alt_names' \
 /etc/ssl/openssl.cnf

cat << EOF >> /etc/ssl/openssl.cnf
[ alt_names ]
IP.1 = 172.31.xxx.xxx
DNS.1 = workstation.k8s.example.com
EOF

CRT=${REGISTRY}/workstation-docker-distribution.crt

openssl req -new -x509 -days 3650 -key $KEY -out $CRT \
 -subj '/CN=workstation.k8s.example.com/O=ORGANIZATION/C=JP'

cd ${REGISTRY}/

scp workstation-docker-distribution.crt controlplane01:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt controlplane02:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt node01:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt node02:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt workstation:/usr/local/share/ca-certificates/

update-ca-certificates

cat > /etc/systemd/system/docker-distribution.service << EOF
[Unit]
Description=docker-distribution Container Registry
After=network.target syslog.target

[Service]
Type=simple
TimeoutStartSec=5m
ExecStartPre=-/usr/bin/podman rm "docker-distribution"

ExecStart=/usr/bin/podman run   --name docker-distribution -p 5000:5000 \
                                -v /opt/registry/data:/var/lib/registry:z \
                                -v /opt/registry/certs:/certs:z \
                                -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/workstation-docker-distribution.crt \
                                -e REGISTRY_HTTP_TLS_KEY=/certs/workstation-docker-distribution.key \
                                docker.io/library/registry:2.8.1

ExecReload=-/usr/bin/podman stop "docker-distribution"
ExecReload=-/usr/bin/podman rm "docker-distribution"
ExecStop=-/usr/bin/podman stop "docker-distribution"
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
EOF

podman pull docker.io/library/registry:2.8.1
systemctl enable --now docker-distribution


apt -y install haproxy
systemctl enable --now haproxy
cp -p /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.org

cat >> /etc/haproxy/haproxy.cfg << EOF
frontend k8s_cp
    bind *:6443
    mode tcp
    option tcplog
    default_backend k8s_cp

backend k8s_cp
    mode tcp
    balance roundrobin
    option tcp-check
    server cp1 controlplane01.k8s.example.com:6443 check
    server cp2 controlpalne02.k8s.example.com:6443 check
EOF

systemctl reload haproxy.service
systemctl status haproxy.service


mkdir -p /etc/kubernetes/audit
cat > /etc/kubernetes/audit/policy.yaml << EOF
kind: Policy
apiVersion: audit.k8s.io/v1
rules:
  - level: Metadata
EOF

cat > ~/kubeadm-config.yaml << EOF
apiVersion: kubeadm.k8s.io/v1beta3 
kind: ClusterConfiguration
kubernetesVersion: v1.29.4
networking:
  podSubnet: 10.244.0.0/16 
apiServer:
  certSANs:
    - "cp.k8s.example.com" 
  extraArgs:
    audit-policy-file: /etc/kubernetes/audit/policy.yaml
    audit-log-path: /var/log/kubernetes/kube-apiserver-audit.log
    audit-log-maxage: "7" 
    audit-log-maxsize: "200" 
  extraVolumes: 
    - name: audit-policy
      hostPath: /etc/kubernetes/audit
      mountPath: /etc/kubernetes/audit
      pathType: DirectoryOrCreate
    - name: "audit-log"
      hostPath: /var/log/kubernetes
      mountPath: /var/log/kubernetes
      pathType: DirectoryOrCreate
controlPlaneEndpoint: "cp.k8s.example.com:6443"
---
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
cgroupDriver: systemd
EOF

kubeadm init --config ~/kubeadm-config.yaml --upload-certs 2>&1 | tee k8s-cluster-install.log

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://gitlab.com/t-tashibu/yaml/-/raw/main/weave.yaml

apt-get update && apt-get install -y apt-transport-https curl
mkdir /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt update
apt install -y kubectl

mkdir -p /root/.kube
scp controlplane01:/etc/kubernetes/admin.conf /root/.kube/config
chown 0:0 /root/.kube/config

