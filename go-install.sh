# Goのインストール
sudo add-apt-repository ppa:longsleep/golang-backports -y
sudo apt update
sudo apt install golang　-y

# kube-image-bouncerのインストール
go install github.com/flavio/kube-image-bouncer@latest

# 自己証明書の作成
cd /etc/kubernetes/
openssl req  -nodes -new -x509 -keyout webhook-key.pem -out webhook.pem
openssl req  -nodes -new -x509 -keyout apiserver-client-key.pem -out apiserver-client.pem

# AdmissonControlの設定ファイル作成
cat > /etc/kubernetes/admission_configuration.json << EOF
{
  "imagePolicy": {
     "kubeConfigFile": "/etc/kubernetes/kube-image-bouncer.yml",
     "allowTTL": 50,
     "denyTTL": 50,
     "retryBackoff": 500,
     "defaultAllow": false
  }
}
EOF

# kubeConfigFileの作成
cat > /etc/kubernetes/kube-image-bouncer.yml　<< EOF
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority: /etc/kubernetes/kube-image-bouncer/webhook.pem
    server: https://bouncer.local.lan:1323/image_policy
  name: bouncer_webhook
contexts:
- context:
    cluster: bouncer_webhook
    user: api-server
  name: bouncer_validator
current-context: bouncer_validator
preferences: {}
users:
- name: api-server
  user:
    client-certificate: /etc/kubernetes/kube-image-bouncer/apiserver-client.pem
    client-key:  /etc/kubernetes/kube-image-bouncer/apiserver-client-key.pem
EOF


