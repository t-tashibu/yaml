# Openssl証明書の作成
openssl genrsa -out k8s-user.key 2048

# 作成したOpenssl証明書から公開鍵の作成を行い、User名との紐づけを行う。
openssl req -new -key k8s-user.key -out k8s-user.csr -subj "/CN=k8s-user"

# 作成した公開鍵をBase64に変換して、それを指定のYamlファイルに出力する
cat k8s-user.csr | base64 | tr -d "\n" >> fsutil-1.yaml
cat fsutil-1.yaml | sed "s/^/  request: /"  > fsutil-2.yaml

# kubernetesクラスターの認証局に登録する公開鍵のYamlファイルを作成し、Base64で変換したコードをrequest:として追加する
cat > k8s-user.yaml << EOF
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: k8s-user
spec:
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
  - key encipherment
  - digital signature
EOF

cat fsutil-2.yaml >> k8s-user.yaml

rm fsutil-1.yaml
rm fsutil-2.yaml

# 公開鍵をApplyして、Applyした公開鍵に許可設定を行う
kubectl apply -f k8s-user.yaml
kubectl certificate approve k8s-user

# kubernetesクラスターにはTLS認証のためにPKI証明書が必要なため、認証局に登録した公開鍵のコードからPKI証明書の作成を行う。
kubectl get csr k8s-user -o jsonpath='{.status.certificate}'| base64 -d > k8s-user.crt

# kubeconfigファイルに作成した証明書、登録したUser、PKI証明書の登録を行う。
kubectl config set-credentials k8s-user --client-key=k8s-user.key --client-certificate=k8s-user.crt --embed-certs=true

# kubernetesのコンテキストファイルにUserと使用するkubernetesクラスター名の登録を行う。
kubectl config set-context k8s-user --cluster=kubernetes --user=k8s-user

# ここからは別のRBACのハンズオンを実施するために必要なNameSpaceやPod、ServiceAccountの作成になります。
kubectl create ns rbac-test

kubectl run --image=nginx rbac-pod --namespace=rbac-test

kubectl create sa -n rbac-test rbac-sa
