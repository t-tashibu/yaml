```
1：Tarファイルのダウンロードと解答、バイナリーの実行
prometheus-exec.sh　ファイルをダウンロードしてシェルの実行
⇒　ここでは実行後にPrometheusが動作しているかを「EC2のIPアドレス:9090」で確認する

```

```
2:NameSpace、ServiceAccount、CluserRoleBindingに加えてDaemonSetのスケジューリング

kubectl apply -f https://gitlab.com/t-tashibu/yaml/-/raw/main/prometheus/ds-node-export.yaml
上記を実行する
```

```
3:PrometheusのYamlファイルの中身を変更する
prometheus.yaml　ファイルの中身を変更する（TarファイルでDLしてきたYamlファイル）

 api_serverのIP＆Portは「kubectl cluster-info」で表記された「172.31.35.152:6443（例）」のアドレスを記載

k8s-node01-exporterのbearer_tokenには、Node01にスケジューリングされているDaemonSetのServiceAccountである
Tokenを記載する

```

```
4:IP&9090でPrometheusにアクセスを行い、実際に確認を行うようにする
```
