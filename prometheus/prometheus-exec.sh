# tarファイルのダウンロード
wget -o- https://github.com/prometheus/prometheus/releases/download/v2.48.1/prometheus-2.48.1.linux-amd64.tar.gz

# tarファイルの解凍
tar xzf prometheus-2.48.1.linux-amd64.tar.gz

# Binaryの実行は
./prometheus --config.file=prometheus.yml
