. /etc/os-release

echo "deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" | tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list

wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${VERSION_ID}/Release.key -O- | apt-key add -

apt update -y
apt install -y podman

mkdir -p /opt/registry/certs
mkdir -p /opt/registry/data

REGISTRY=/opt/registry/certs
KEY=${REGISTRY}/workstation-docker-distribution.key
openssl genrsa -out $KEY 2048

sed -i.bak -e \
 '/^\[ v3_ca \]/a subjectAltName=@alt_names' \
 /etc/ssl/openssl.cnf

cat << EOF >> /etc/ssl/openssl.cnf
[ alt_names ]
IP.1 = 172.31.10.33
DNS.1 = workstation.k8s.example.com
EOF

CRT=${REGISTRY}/workstation-docker-distribution.crt
openssl req -new -x509 -days 3650 -key $KEY -out $CRT \
 -subj '/CN=workstation.k8s.example.com/O=ORGANIZATION/C=JP'

cd ${REGISTRY}/

scp workstation-docker-distribution.crt cp1:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt cp2:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt cp3:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt worker1:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt worker2:/usr/local/share/ca-certificates/
scp workstation-docker-distribution.crt workstation:/usr/local/share/ca-certificates/

update-ca-certificates


podman pull docker.io/library/registry:2.8.1
systemctl enable --now docker-distribution
systemctl status podman

apt -y install haproxy
systemctl enable --now haproxy
cp -p /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.org

cat >> /etc/haproxy/haproxy.cfg << EOF
frontend k8s_cp
    bind *:6443
    mode tcp
    option tcplog
    default_backend k8s_cp

backend k8s_cp
    mode tcp
    balance roundrobin
    option tcp-check
    server cp1 cp1.k8s.example.com:6443 check
    server cp2 cp2.k8s.example.com:6443 check
    server cp3 cp3.k8s.example.com:6443 check
EOF

systemctl reload haproxy.service
systemctl status haproxy.service