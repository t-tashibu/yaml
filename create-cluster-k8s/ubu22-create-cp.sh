# Swapの無効化
swapoff -a

# エラー対処
apt purge needrestart -y

# カーネルパラメータ設定
cat > /etc/modules-load.d/containerd.conf << EOF
overlay
br_netfilter
EOF

modprobe overlay
# 間を入れる
modprobe br_netfilter

cat > /etc/sysctl.d/99-kubernetes-cri.conf << EOF
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

# 間を入れる
sysctl --system

# ランタイムのインストール
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
# 間を入れる
add-apt-repository -y \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

# 間を入れる
apt update && apt install -y containerd.io
# 間を入れる
mkdir -p /etc/containerd
containerd config default | tee /etc/containerd/config.toml
# 間を入れる
systemctl restart containerd

# ubuntu22.04 不具合対応
sed -i "s/SystemdCgroup = false/SystemdCgroup = true/g" /etc/containerd/config.toml
sed -i "s#sandbox_image = "registry.k8s.io/pause:3.6"#sandbox_image = "registry.k8s.io/pause:3.9"#g" /etc/containerd/config.toml
systemctl restart containerd

# kubeadm、kubelet、kubectlのインストール
apt update && apt install -y apt-transport-https ca-certificates curl gpg
sudo mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list

apt update
apt install -y kubelet=1.29.0-1.1 kubeadm=1.29.0-1.1 kubectl=1.29.0-1.1

kubeadm init --pod-network-cidr=10.244.0.0/16 2>&1 | tee k8s-cluster-install.log

mkdir -p /root/.kube 
cp -i /etc/kubernetes/admin.conf /root/.kube/config 
chown $(id -u):$(id -g) /root/.kube/config
export KUBECONFIG=/root/.kube/config

kubectl apply -f https://gitlab.com/t-tashibu/yaml/-/raw/main/weave.yaml

source <(kubectl completion bash) 
echo "source <(kubectl completion bash)" >> ~/.bashrc
alias k=kubectl
complete -F __start_kubectl k
