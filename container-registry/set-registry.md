```
cat > /etc/systemd/system/docker-distribution.service << EOF
[Unit]
Description=docker-distribution Container Registry
After=network.target syslog.target

[Service]
Type=simple
TimeoutStartSec=5m
ExecStartPre=-/usr/bin/podman rm "docker-distribution"

ExecStart=/usr/bin/podman run   --name docker-distribution -p 5000:5000 \
                                -v /opt/registry/data:/var/lib/registry:z \
                                -v /opt/registry/certs:/certs:z \
                                -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/controlplane-docker-distribution.crt \
                                -e REGISTRY_HTTP_TLS_KEY=/certs/controlplane-docker-distribution.key \
                                docker.io/library/registry:2.8.1

ExecReload=-/usr/bin/podman stop "docker-distribution"
ExecReload=-/usr/bin/podman rm "docker-distribution"
ExecStop=-/usr/bin/podman stop "docker-distribution"
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
EOF
```